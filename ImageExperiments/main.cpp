//
//  main.cpp
//  ImageExperiments
//
//  Created by Marty on 02/10/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#include <iostream>
#include <vector>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>

Uint32 getpixel(SDL_Surface *surface, int x, int y);
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);


void experiment1(Uint32* originalSurfacePixels, int width, int height, int pitch)
{
    Uint32* pixels = originalSurfacePixels;
    
    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            pixels[i * width + j] = pixels[i + width * j];
        }
    }
    
    SDL_Surface* experiment = SDL_CreateRGBSurfaceFrom(pixels, width, height, 32, pitch, 0, 0, 0, 0);
    IMG_SavePNG(experiment, "experiment1.png");
    SDL_FreeSurface(experiment);
}


void experiment2(Uint32* originalSurfacePixels, int width, int height, int pitch)
{
    Uint32* pixels = originalSurfacePixels;
    
    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            pixels[i * width + j] = pixels[(800 - i) * width + (800 - j)];
        }
    }
    
    SDL_Surface* experiment = SDL_CreateRGBSurfaceFrom(pixels, width, height, 32, pitch, 0, 0, 0, 0);
    IMG_SavePNG(experiment, "experiment2.png");
    SDL_FreeSurface(experiment);
}


void experiment3(Uint32* originalSurfacePixels, int width, int height, int pitch)
{
    Uint32* pixels = originalSurfacePixels;
    
    for (int i = 1; i < width; i++)
    {
        for (int j = 1; j < height; j++)
        {
            pixels[i * width + j] = pixels[(i - 1) * width + (j - 1)];
        }
    }
    
    SDL_Surface* experiment = SDL_CreateRGBSurfaceFrom(pixels, width, height, 32, pitch, 0, 0, 0, 0);
    IMG_SavePNG(experiment, "experiment3.png");
    SDL_FreeSurface(experiment);
}


void experiment4(Uint32* originalSurfacePixels, int width, int height, int pitch)
{
    Uint32* pixels = originalSurfacePixels;
    
    for (int i = 10; i < width; i++)
    {
        for (int j = 10; j < height; j++)
        {
            Uint32 firstPixel = pixels[(i - 10) * width + (j - 10)];
            Uint32 currentPixel = pixels[i * width + j];
            Uint32 newPixel = (firstPixel + currentPixel) / 2;
            pixels[i * width + j] = newPixel;
        }
    }
    
    SDL_Surface* experiment = SDL_CreateRGBSurfaceFrom(pixels, width, height, 32, pitch, 0, 0, 0, 0);
    IMG_SavePNG(experiment, "experiment4.png");
    SDL_FreeSurface(experiment);
}


void experiment5(Uint32* originalSurfacePixels, int width, int height, int pitch)
{
    Uint32* pixels = originalSurfacePixels;
    
    std::vector<Uint32> pixelVector;
    
    for (int i = 0; i < width; ++i)
    {
        for (int j = 0; j < height; ++j)
        {
            pixelVector.push_back(pixels[i * width + j]);
        }
    }
    
    std::sort(pixelVector.begin(), pixelVector.end());
    int k = 0;
    for (int i = 0; i < width; ++i)
    {
        for (int j = 0; j < height; ++j)
        {
            pixels[i * width + j] = pixelVector[k];
            ++k;
        }
    }
    
    SDL_Surface* experiment = SDL_CreateRGBSurfaceFrom(pixels, width, height, 32, pitch, 0, 0, 0, 0);
    IMG_SavePNG(experiment, "experiment5.png");
    SDL_FreeSurface(experiment);
}


void experiment6(Uint32* originalSurfacePixels, int width, int height, int pitch)
{
    Uint32* pixels = originalSurfacePixels;
    
    Uint32* pixels2 = pixels;
    
    int first = std::rand() % 800;
    int second = std::rand() % 800;
    
    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; ++j)
        {
            originalSurfacePixels[first * width + j] = originalSurfacePixels[second * width + j];
            
            first = std::rand() % 500;
            second = std::rand() % 600;
        }
       
    }

    
    SDL_Surface* experiment = SDL_CreateRGBSurfaceFrom(originalSurfacePixels, width, height, 32, pitch, 0, 0, 0, 0);
    IMG_SavePNG(experiment, "experiment6.png");
    SDL_FreeSurface(experiment);
}


void experiment7(Uint32* originalSurfacePixels, int width, int height, int pitch)
{
    Uint32* pixels = originalSurfacePixels;
    
    Uint32 pixels2[800][800];
    
    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; ++j)
        {
            pixels2[i][j] = originalSurfacePixels[i * width + j];
        }
    }
    
    Uint32 pixels3[800][800];
    int first = std::rand() % 800;
    int second = std::rand() % 800;
    
    for (int i = 0; i < 800; ++i)
    {
        for (int j = 0; j < 800; j+=5)
        {
            pixels3[i][j] = pixels2[i][second];
            first = std::rand() % 800;
            second = std::rand() % 800;
        }
    }
    
    
    
    SDL_Surface* experiment = SDL_CreateRGBSurfaceFrom(pixels3, width, height, 32, pitch, 0, 0, 0, 0);
    IMG_SavePNG(experiment, "experiment7.png");
    SDL_FreeSurface(experiment);
}


void experiment8(Uint32* originalSurfacePixels, int width, int height, int pitch)
{
	Uint32* pixels = originalSurfacePixels;
	
	Uint32 pixels2[800][800];
	SDL_Surface* experiment = SDL_CreateRGBSurfaceFrom(pixels2, width, height, 32, pitch, 0, 0, 0, 0);
	
	
	for (int i = 0; i < width ; i+= 40)
	{
		for (int j = 0; j < height ; j+= 40)
		{
			
			
			Uint32 average = 0;
			for (int x = i; x < i + 20; ++x) {
				for (int y = j; y < j + 20; ++y) {
					
					
					Uint32 val = pixels[x * width + y];
					val = getpixel(experiment, x, y);
					
					average += val;
					
					//pixels2[x][y] = originalSurfacePixels[i * width + j];
				}
			}
			
			average = average / 400;
			
			for (int x = i; x < i + 40; ++x) {
				for (int y = j; y < j + 40; ++y) {
					
					
					pixels2[x][y] = average;
					
					
					
				}
			}
			average = 0;
		}
	}
	
	
	
	IMG_SavePNG(experiment, "experiment8.png");
	SDL_FreeSurface(experiment);
}

Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
	int bpp = surface->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to retrieve */
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
	
	switch(bpp) {
		case 1:
			return *p;
			break;
			
		case 2:
			return *(Uint16 *)p;
			break;
			
		case 3:
			if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
				return p[0] << 16 | p[1] << 8 | p[2];
			else
				return p[0] | p[1] << 8 | p[2] << 16;
			break;
			
		case 4:
			return *(Uint32 *)p;
			break;
			
		default:
			return 0;       /* shouldn't happen, but avoids warnings */
	}
}
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
	int bpp = surface->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to set */
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
	
	switch(bpp) {
		case 1:
			*p = pixel;
			break;
			
		case 2:
			*(Uint16 *)p = pixel;
			break;
			
		case 3:
			if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
				p[0] = (pixel >> 16) & 0xff;
				p[1] = (pixel >> 8) & 0xff;
				p[2] = pixel & 0xff;
			} else {
				p[0] = pixel & 0xff;
				p[1] = (pixel >> 8) & 0xff;
				p[2] = (pixel >> 16) & 0xff;
			}
			break;
			
		case 4:
			*(Uint32 *)p = pixel;
			break;
	}
}


int main(int argc, const char * argv[]) {
	
    SDL_Init(SDL_INIT_VIDEO);
	
    // Load image in
    SDL_Surface* image = IMG_Load("glencoe.jpg");
    
    
    int width = image->w;
    int height = image->h;
    int pitch = image->pitch;
    Uint32 * pixels1 = (Uint32 *)image->pixels;
    Uint32 * pixels2 = pixels1;
    Uint32 * pixels3 = pixels1;
    Uint32 * pixels4 = pixels1;
    Uint32 * pixels5 = pixels1;
    Uint32 * pixels6 = pixels1;
	Uint32 * pixels7 = pixels1;
	Uint32 * pixels8 = pixels1;
    
    
    
    experiment1(pixels1, width, height, pitch);
    experiment2(pixels2, width, height, pitch);
    experiment3(pixels3, width, height, pitch);
    experiment4(pixels4, width, height, pitch);
    experiment5(pixels5, width, height, pitch);
    experiment6(pixels6, width, height, pitch);
    experiment7(pixels7, width, height, pitch);
	experiment8(pixels8, width, height, pitch);
    
    return 0;
}
